from django.contrib import admin

from .models import Voting, Comment

admin.site.register(Voting)
admin.site.register(Comment)
