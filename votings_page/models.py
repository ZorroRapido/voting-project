from django.contrib.auth.models import User
from django.db import models


class Voting(models.Model):
    voting_name = models.CharField('Название голосования', max_length=100)
    voting_question = models.CharField('Вопрос голосования', max_length=255)  # Вопрос голосования
    voting_type = models.CharField('', max_length=255)
    votes_number = models.IntegerField('Число голосов', default=0)  # Общее число голосов
    author = models.ForeignKey(User, on_delete=models.CASCADE)  # Автор голосования
    pub_date = models.DateTimeField('Дата создания')  # Дата создания голосования
    commentary_number = models.IntegerField('Кол-во комментариев', default=0)  # Кол-во комментариев под голосованием
    variants = models.CharField('', max_length=2048)
    num_of_voters = models.CharField('', max_length=2048)
    id_of_voters = models.CharField('', max_length=2048)
    complaints = models.CharField('', max_length=2048)  # Жалобы на голосование

    def __str__(self):
        return self.voting_name

    class Meta:
        verbose_name = "Голосование"
        verbose_name_plural = "Голосования"


class Comment(models.Model):
    voting = models.ForeignKey(Voting, on_delete=models.CASCADE)  # Голосование, к которому привязан комментарий
    author_name = models.ForeignKey(User, on_delete=models.CASCADE)  # Имя автора комментария
    # author_name = models.CharField('Имя автора', max_length=50)  # Имя автора комментария
    comment_text = models.CharField('Текст комментария', max_length=255)  # Текст комментария

    def __str__(self):
        return self.author_name

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"
