from django.urls import path
from . import views

app_name = "votings"

urlpatterns = [
    path('', views.voting_page, name='voting_page'),
    path('<int:voting_id>/', views.detail, name='detail'),
    path('<int:voting_id>/add_comment/', views.add_comment, name='add_comment'),
    path('createvote/', views.create_vote, name='create_voting_page'),
    path('<int:voting_id>/editevote/', views.edit_vote, name='edit_vote'),
    path('<int:voting_id>/complaint/', views.complaint, name='complaint'),
    path('<int:voting_id>/showcomplaints/', views.show_complaints, name='showcomplaints'),
]
