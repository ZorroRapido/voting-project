from django import forms

class UserForm(forms.Form):
    voting_name = forms.CharField('Название голосования', max_length=100)
    voting_question = forms.CharField('Вопрос голосования', max_length=255)  # Вопрос голосования
    votes_number = forms.CharField('Число голосов', max_length=16)  # Общее число голосов
    commentary_number = forms.CharField('Кол-во комментариев', max_length=16)  # Кол-во комментариев под голосованием
