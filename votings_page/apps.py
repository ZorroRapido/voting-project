from django.apps import AppConfig


class VotingsPageConfig(AppConfig):
    name = 'votings_page'
    verbose_name = "Голосования"
