from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .models import Voting
from django.template import RequestContext
from django.utils import timezone


def voting_page(request):
    votings_list = Voting.objects.all()
    context = {'votings_list': votings_list}

    return render(request, "votings.html", context)  # , RequestContext(request))


def detail(request, voting_id):
    try:
        vote = Voting.objects.get(id=voting_id)
    except Exception:
        raise Http404("Голосование не найдено!")

    if request.method == 'POST':
        if 'delete vote' in request.POST:
            Voting.objects.filter(id=voting_id).delete()
            return HttpResponseRedirect("/votings")
        elif 'send' in request.POST:
            variants = vote.variants.split('@#$*')
            check = []
            if vote.voting_type == '2':
                for i in variants:
                    check.append(request.POST.get(i, "none"))
            else:
                check.append(request.POST.get("checkboxes", "none"))
            users = vote.id_of_voters.split(',')
            if str(request.user.id) not in users:
                for i in range(len(variants)):
                    for j in check:
                        if variants[i] == j:
                            if i == 0:
                                frontstr = ''
                            else:
                                frontstr = vote.num_of_voters[:((i * 5) - 0)]
                            backstr = vote.num_of_voters[((i * 5) + 1):]
                            vote.num_of_voters = \
                                frontstr + str(int(vote.num_of_voters[i * 5]) + 1) + backstr
                            if str(request.user.id) not in vote.id_of_voters.split(','):
                                vote.id_of_voters += str(request.user.id) + ','
                                vote.votes_number += 1
                            vote.save()

    latest_comments_list = vote.comment_set.order_by('-id')[:10]  # Последние 10 комментариев
    variants = vote.variants.split('@#$*')
    num_of_voters = []
    for i in range(len(variants)):
        num_of_voters.append('За вариант ' + str(i + 1)
                             + ' проголосовало: ' + vote.num_of_voters.split('@#$*')[i])

    context = {'voting': vote, 'latest_comments_list': latest_comments_list, 'variants': variants,
               'num_of_voters': num_of_voters, 'author': False}

    if request.user.id == vote.author.id:  # проверка: является ли пользователь автором
        context['author'] = True

    return render(request, "detail.html", context)  # RequestContext(request))


@login_required
def add_comment(request, voting_id):
    try:
        a = Voting.objects.get(id=voting_id)
    except:
        return Http404("Голосование не найдено!")

    a.comment_set.create(author_name=request.user, comment_text=request.POST['text'])
    # a.comment_set.create(author_name=request.POST['name'], comment_text=request.POST['text'])
    a.commentary_number = a.comment_set.count()
    a.save()

    return HttpResponseRedirect(reverse('votings:detail', args=(a.id,)))


@login_required
def create_vote(request):
    context = {}
    if request.method == 'POST':
        variants = request.POST.get("variants", "none")
        variant1 = request.POST.get("variant1", "none")
        variant2 = request.POST.get("variant2", "none")
        variants3 = request.POST.get("variant3", "none")
        type = '1'
        if variants == '':
            if variants3 != '':
                variants = variants3
                type = '2'
            elif variant1 != '' and variant2 != '':
                variants = variant1 + ',' + variant2
                type = '3'
            else:
                return render(request, "createvote.html", context)
        num_of_voters = '0@#$*'
        for i in variants:
            if i == ',':
                num_of_voters += '0@#$*'
        variants = variants.replace(', ', '@#$*')
        variants = variants.replace(',', '@#$*')

        voting = Voting(
            voting_name=request.POST.get("voting_name", "none"),
            voting_question=request.POST.get("voting_question", "none"),
            voting_type=type,
            votes_number=0,  # Общее число голосов
            author=request.user,  # Автор голосования
            pub_date=timezone.localtime(timezone.now()).strftime("%Y-%m-%d %H:%M"),  # Дата создания голосования
            commentary_number=0,
            variants=variants,
            num_of_voters=num_of_voters,
            id_of_voters='',
        )
        voting.save()
        return HttpResponseRedirect('/votings/')
    return render(request, "createvote.html", context)


@login_required
def edit_vote(request, voting_id):
    try:
        vote = Voting.objects.get(id=voting_id)
        if vote.author.id != request.user.id:  # проверка: является ли пользователь автором
            return HttpResponseRedirect('/votings/')
        context = {'voting': vote}
        if request.method == 'POST':
            variants = request.POST.get("variants", "none")
            variant1 = request.POST.get("variant1", "none")
            variant2 = request.POST.get("variant2", "none")
            variants3 = request.POST.get("variant3", "none")
            type = '1'
            if (variants == ''):
                if (variants3 != ''):
                    variants = variants3
                    type = '2'
                elif (variant1 != '' and variant2 != ''):
                    variants = variant1 + ',' + variant2
                    type = '3'
                else:
                    return HttpResponseRedirect('/votings/' + str(voting_id) + '/')
            num_of_voters = '0@#$*'
            for i in variants:
                if (i == ','):
                    num_of_voters += '0@#$*'
            variants = variants.replace(', ', '@#$*')
            variants = variants.replace(',', '@#$*')
            print(variants)
            voting = Voting(
                voting_name=request.POST.get("voting_name", "none"),
                voting_question=request.POST.get("voting_question", "none"),
                voting_type=type,
                votes_number=0,
                author=vote.author,  # Автор голосования
                pub_date=vote.pub_date,  # Дата создания голосования
                commentary_number=vote.commentary_number,
                variants=variants,
                num_of_voters=num_of_voters,
                id_of_voters='',
            )
            vote.voting_name = voting.voting_name
            vote.voting_question = voting.voting_question
            vote.votes_number = voting.votes_number
            vote.commentary_number = voting.commentary_number
            vote.variants = voting.variants
            vote.num_of_voters = voting.num_of_voters
            vote.id_of_voters = ''
            vote.voting_type = voting.voting_type
            vote.votes_number = 0
            vote.save()
            return HttpResponseRedirect('/votings/' + str(voting_id) + '/')
    except Voting.DoesNotExist:
        return Http404("Голосование не найдено!")
    return render(request, "editvote.html", context)


def complaint(request, voting_id):
    context = {}
    try:
        vote = Voting.objects.get(id=voting_id)
    except Voting.DoesNotExist:
        return Http404("Голосование не найдено!")
    if request.method == 'POST':
        complaints = request.POST.get("complaint", "none")
        if complaints != 'none' and complaints != '':
            vote.complaints += complaints + '@#$*'
            vote.save()
            return HttpResponseRedirect('/votings/' + str(voting_id) + '/')
    return render(request, "complainvote.html", context)


def show_complaints(request, voting_id):
    context = {}
    try:
        vote = Voting.objects.get(id=voting_id)
    except Voting.DoesNotExist:
        return Http404("Голосование не найдено!")
    if vote.author.id != request.user.id:  # проверка: является ли пользователь автором
        return HttpResponseRedirect('/votings/')
    complaints = vote.complaints.split('@#$*')
    for i in range(len(complaints)):
        if complaints[i] == '':
            del complaints[i]
    if not complaints:
        empty = True
    else:
        empty = False
    context = {
        'complaints': complaints,
        'empty': empty,
        'voting': vote
    }
    return render(request, "showcomplaints.html", context)
