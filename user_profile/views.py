from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render

from votings_page.models import Voting
from .forms import RegistrationForm
from django.http import Http404, HttpResponseRedirect
from django.contrib.auth.hashers import make_password
#
from django.contrib import messages


#


def register(request):
    context = {}

    if request.method == "POST":
        registration_data = RegistrationForm(request.POST)

        if registration_data.is_valid():
            # Обработка данных, полученных через форму "Регистрация"
            context['username'] = registration_data.data['username']
            context['password'] = registration_data.data['password']
            context['email'] = registration_data.data['email']

            # Проверяем, нет ли пользователя с таким логином в БД
            if len(User.objects.filter(username=registration_data.data['username'])) == 0:
                context['username_error'] = False
            else:
                context['username_error'] = True
                #
                messages.error(request, "Ошибка! Пользователь с таким username уже зарегистрирован!")
                #

            if len(User.objects.filter(email=context['email'])) == 0:
                context['email_error'] = False
            else:
                context['email_error'] = True
                #
                messages.error(request, "Ошибка! Пользователь с таким e-mail уже зарегистрирован!")
                #

            if context['username_error'] is False and context['email_error'] is False:
                #
                messages.success(request, "Вы успешно зарегистрировались!")
                #

                # new_user = registration_data.save(commit=False)

                new_user = User.objects.create(username=context['username'],
                                               email=context['email'])

                # new_user = User(username=context['username'],
                #    password=context['password'],
                # email=context['email'])

                new_user.set_password(registration_data.cleaned_data['password'])

                # new_user.password = make_password(new_user.password)

                new_user.save()

        # context['form'] = RegistrationForm()
    else:
        context['username'] = ""
        context['password'] = ""
        context['form'] = RegistrationForm()
        context['username_error'] = False
        context['email_error'] = False

    return render(request, 'registration.html', context)


@login_required
def profile(request, name):
    try:
        user = User.objects.get(username=name)
    except User.DoesNotExist:
        raise Http404("Такого пользователя не существует!")
    user_votings = Voting.objects.filter(author=user)
    nums_of_votings = len(user_votings)
    nums_of_votes = 0
    votings = Voting.objects.all()
    for i in votings:
        print(i.id_of_voters.split(','))
        if str(user.id) in i.id_of_voters.split(','):
            nums_of_votes += 1
    context = {
        'user' : user,
        'nums_of_votings' : nums_of_votings,
        'nums_of_votes' : nums_of_votes,
        'votings_list' : user_votings
    }
    return render(request, 'user_profile.html', context)



def index_page(request):
    return HttpResponseRedirect("/votings")
