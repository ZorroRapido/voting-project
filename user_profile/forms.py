from django import forms


class RegistrationForm(forms.Form):  # Форма регистрации новых пользователей
    username = forms.CharField(label='Username', max_length=255)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, max_length=50)
    email = forms.EmailField(label='E-mail', max_length=50)
