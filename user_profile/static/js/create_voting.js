function openType(num)
{
    closeAll();
    if (num == 1)
    {
        let divElem = document.querySelector('.mydiv1');
        divElem.style.display = "";
    }
    else if (num == 2)
    {
        let divElem = document.querySelector('.mydiv2');
        divElem.style.display = "";
    }
    else
    {
        let divElem = document.querySelector('.mydiv3');
        divElem.style.display = "";
    }
}

function closeAll()
{
    let divElem = document.querySelector('.mydiv1');
    divElem.style.display = "none";
    divElem = document.querySelector('.mydiv2');
    divElem.style.display = "none";
    divElem = document.querySelector('.mydiv3');
    divElem.style.display = "none";
}